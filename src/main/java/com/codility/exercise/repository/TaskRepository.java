package com.codility.exercise.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.codility.exercise.domain.Task;

/**
 * @author Roberto Noriega
 * @since 08.01.2021
 * @version 1.0.0
 */
public interface TaskRepository extends JpaRepository<Task, Long> {
}