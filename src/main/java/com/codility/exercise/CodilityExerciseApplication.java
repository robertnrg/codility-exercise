package com.codility.exercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodilityExerciseApplication {

   public static void main(String[] args) {
      SpringApplication.run(CodilityExerciseApplication.class, args);
   }

}
