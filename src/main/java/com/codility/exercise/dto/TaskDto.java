package com.codility.exercise.dto;

/**
 * @author Roberto Noriega
 * @since 08.01.2021
 * @version 1.0.0
 */
public class TaskDto implements java.io.Serializable {

   private String  description;
   private Integer priority;

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public Integer getPriority() {
      return priority;
   }

   public void setPriority(Integer priority) {
      this.priority = priority;
   }

}