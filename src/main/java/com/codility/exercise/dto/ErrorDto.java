package com.codility.exercise.dto;

/**
 * @author Roberto Noriega
 * @since 08.01.2021
 * @version 1.0.0
 */
public class ErrorDto implements java.io.Serializable {

   private final String  message;
   private final Integer status;

   public ErrorDto(String message, Integer status) {
      this.message = message;
      this.status = status;
   }

   public String getMessage() {
      return message;
   }

   public Integer getStatus() {
      return status;
   }

}
