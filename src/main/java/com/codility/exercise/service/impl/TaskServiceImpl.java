package com.codility.exercise.service.impl;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.codility.exercise.domain.Task;
import com.codility.exercise.dto.TaskDto;
import com.codility.exercise.exception.PreConditionFailedException;
import com.codility.exercise.repository.TaskRepository;
import com.codility.exercise.service.TaskService;

/**
 * @author Roberto Noriega
 * @since 08.01.2021
 * @version 1.0.0
 */
@Service
public class TaskServiceImpl implements TaskService {

   private final TaskRepository taskRepository;

   public TaskServiceImpl(TaskRepository taskRepository) {
      this.taskRepository = taskRepository;
   }

   @Override
   public TaskDto update(Long id, TaskDto taskDto) {
      if (!StringUtils.hasText(taskDto.getDescription())) {
         throw new PreConditionFailedException("Task description is required");
      }
      Task taskToUpdate = taskRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Cannot find task with given id"));
      BeanUtils.copyProperties(taskDto, taskToUpdate);
      taskRepository.saveAndFlush(taskToUpdate);

      return taskDto;
   }

}
