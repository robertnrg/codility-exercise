package com.codility.exercise.service;

import com.codility.exercise.dto.TaskDto;

/**
 * @author Roberto Noriega
 * @since 08.01.2021
 * @version 1.0.0
 */
public interface TaskService {

   TaskDto update(Long id, TaskDto taskDto);

}