package com.codility.exercise.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Roberto Noriega
 * @since 08.01.2021
 * @version 1.0.0
 */
@Entity
public class Task implements java.io.Serializable {

   @Id
   private Long   id;

   @Column(length = 200, nullable = false)
   private String description;

   @Column(columnDefinition = "bigint", nullable = false)
   private Long   priority;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public Long getPriority() {
      return priority;
   }

   public void setPriority(Long priority) {
      this.priority = priority;
   }

}