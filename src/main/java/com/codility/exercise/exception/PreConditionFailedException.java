package com.codility.exercise.exception;

/**
 * @author Roberto Noriega
 * @since 08.01.2021
 * @version 1.0.0
 */
public class PreConditionFailedException extends RuntimeException {

   private static final long serialVersionUID = -7043944113105756846L;

   public PreConditionFailedException(String message) {
      super(message);
   }

}
