package com.codility.exercise.controller;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.codility.exercise.dto.ErrorDto;
import com.codility.exercise.exception.PreConditionFailedException;

/**
 * @author Roberto Noriega
 * @since 08.01.2021
 * @version 1.0.0
 */
@RestControllerAdvice
public class ExceptionHandlerController {

   @ResponseStatus(code = HttpStatus.NOT_FOUND)
   @ExceptionHandler(value = EntityNotFoundException.class)
   public ErrorDto handleEntityNotFound(EntityNotFoundException entityNotFoundException) {
      return new ErrorDto(entityNotFoundException.getMessage(), HttpStatus.NOT_FOUND.value());
   }

   @ResponseStatus(code = HttpStatus.BAD_REQUEST)
   @ExceptionHandler(value = PreConditionFailedException.class)
   public ErrorDto handlePreConditionFailedException(PreConditionFailedException preConditionFailedException) {
      return new ErrorDto(preConditionFailedException.getMessage(), HttpStatus.BAD_REQUEST.value());
   }

}