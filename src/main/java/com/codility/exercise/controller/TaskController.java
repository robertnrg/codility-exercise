package com.codility.exercise.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codility.exercise.dto.TaskDto;
import com.codility.exercise.service.TaskService;

/**
 * @author Roberto Noriega
 * @since 08.01.2021
 * @version 1.0.0
 */
@RestController
@RequestMapping(path = "/task")
public class TaskController {

   private final TaskService taskService;

   public TaskController(TaskService taskService) {
      this.taskService = taskService;
   }

   @ResponseStatus(code = HttpStatus.OK)
   @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
   public TaskDto update(@PathVariable Long id, @RequestBody TaskDto taskDto) {
      return this.taskService.update(id, taskDto);
   }

}