package com.codility.exercise;

import com.codility.exercise.dto.TaskDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CodilityExerciseApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Test
	@DisplayName(value = "should_return_not_found_message_when_id_not_exist")
	void shouldReturnNotFoundMessageWhenIdNotExist() throws Exception {
		LOGGER.info("should_return_not_found_message_when_id_not_exist");
		long taskId = 10;
		TaskDto taskDto = new TaskDto();
		taskDto.setDescription("new description");
		taskDto.setPriority(15);
		this.mockMvc.perform(put("/task/" + taskId)
			.contentType(MediaType.APPLICATION_JSON_VALUE)
			.accept(MediaType.APPLICATION_JSON_VALUE)
			.characterEncoding(StandardCharsets.UTF_8.name())
			.content(this.objectMapper.writeValueAsBytes(taskDto)))
			.andDo(print())
			.andExpect(status().isNotFound())
			.andExpect(jsonPath("$.message").value("Cannot find task with given id"))
			.andExpect(jsonPath("$.status").value(404));
	}

	@Test
	@DisplayName(value = "should_return_bad_request_status_when_description_is_empty")
	void shouldReturnBadRequestStatusWhenDescriptionIsEmpty() throws Exception {
		LOGGER.info("should_return_bad_request_status_when_description_is_empty");
		long taskId = 1;
		TaskDto taskDto = new TaskDto();
		taskDto.setDescription("");
		taskDto.setPriority(15);
		this.mockMvc.perform(put("/task/" + taskId)
			.contentType(MediaType.APPLICATION_JSON_VALUE)
			.accept(MediaType.APPLICATION_JSON_VALUE)
			.characterEncoding(StandardCharsets.UTF_8.name())
			.content(this.objectMapper.writeValueAsBytes(taskDto)))
			.andDo(print())
			.andExpect(status().isBadRequest());
	}

	@Test
	@DisplayName(value = "should_update_task")
	void shouldUpdateTask() throws Exception {
		LOGGER.info("should_update_task");
		long taskId = 1;
		TaskDto taskDto = new TaskDto();
		taskDto.setDescription("new description");
		taskDto.setPriority(15);
		this.mockMvc.perform(put("/task/" + taskId)
			.contentType(MediaType.APPLICATION_JSON_VALUE)
			.accept(MediaType.APPLICATION_JSON_VALUE)
			.characterEncoding(StandardCharsets.UTF_8.name())
			.content(this.objectMapper.writeValueAsBytes(taskDto)))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.id").value(1))
			.andExpect(jsonPath("$.description").value("new description"))
			.andExpect(jsonPath("$.priority").value(15));;
	}

	@Test
	@DisplayName(value = "should_update_task_and_return_task_details_in_body")
	void shouldUpdateTaskAndReturnTaskDetailsInBody() throws Exception {
		LOGGER.info("should_update_task_and_return_task_details_in_body");
		long taskId = 1;
		TaskDto taskDto = new TaskDto();
		taskDto.setDescription("new description");
		taskDto.setPriority(15);
		this.mockMvc.perform(put("/task/" + taskId)
			.contentType(MediaType.APPLICATION_JSON_VALUE)
			.accept(MediaType.APPLICATION_JSON_VALUE)
			.characterEncoding(StandardCharsets.UTF_8.name())
			.content(this.objectMapper.writeValueAsBytes(taskDto)))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.description").value("new description"))
			.andExpect(jsonPath("$.priority").value(15));
	}

	@Test
	@DisplayName(value = "should_return_bad_request_message_when_description_is_empty")
	void shouldReturnBadRequestMessageWhenDescriptionIsEmpty() throws Exception {
		LOGGER.info("should_return_bad_request_message_when_description_is_empty");
		long taskId = 1;
		TaskDto taskDto = new TaskDto();
		taskDto.setDescription("");
		taskDto.setPriority(15);
		this.mockMvc.perform(put("/task/" + taskId)
			.contentType(MediaType.APPLICATION_JSON_VALUE)
			.accept(MediaType.APPLICATION_JSON_VALUE)
			.characterEncoding(StandardCharsets.UTF_8.name())
			.content(this.objectMapper.writeValueAsBytes(taskDto)))
			.andDo(print())
			.andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.message").value("Task description is required"))
			.andExpect(jsonPath("$.status").value(400));
	}

	@Test
	@DisplayName(value = "should_update_task_entity")
	void shouldUpdateTaskEntity() throws Exception {
		LOGGER.info("should_update_task_entity");
		long taskId = 1;
		TaskDto taskDto = new TaskDto();
		taskDto.setDescription("new description");
		taskDto.setPriority(15);
		this.mockMvc.perform(put("/task/" + taskId)
			.contentType(MediaType.APPLICATION_JSON_VALUE)
			.accept(MediaType.APPLICATION_JSON_VALUE)
			.characterEncoding(StandardCharsets.UTF_8.name())
			.content(this.objectMapper.writeValueAsBytes(taskDto)))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.priority").value(15));;
	}

}