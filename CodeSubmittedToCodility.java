package com.codility.tasks.hibernate.solution;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.EntityNotFoundException;
import org.springframework.data.jpa.repository.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.*;

@Entity
class Task implements java.io.Serializable {

     @Id
     private Long id;

     @Column(length = 200, nullable = false)
     private String description;

     @Column(columnDefinition = "bigint", nullable = false)
     private Long priority;

     public Long getId() {
          return id;
     }

     public void setId(Long id) {
          this.id = id;
     }

     public String getDescription() {
          return description;
     }

     public void setDescription(String description) {
          this.description = description;
     }

     public Long getPriority() {
          return priority;
     }

     public void setPriority(Long priority) {
          this.priority = priority;
     }

}

class TaskDto implements java.io.Serializable {

     private String description;
     private Integer priority;

     public String getDescription() {
          return description;
     }

     public void setDescription(String description) {
          this.description = description;
     }

     public Integer getPriority() {
          return priority;
     }

     public void setPriority(Integer priority) {
          this.priority = priority;
     }

}

class ErrorDto implements java.io.Serializable {

     private final String message;
     private final Integer status;

     public ErrorDto(String message, Integer status) {
          this.message = message;
          this.status = status;
     }

     public String getMessage() {
          return message;
     }

     public Integer getStatus() {
          return status;
     }

}

@RestController
@RequestMapping(path = "/task")
class TaskController {

     private final TaskService taskService;

     public TaskController(TaskService taskService) {
          this.taskService = taskService;
     }

     @ResponseStatus(code = HttpStatus.OK)
     @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
     public TaskDto update(@PathVariable Long id, @RequestBody TaskDto taskDto) {
          return this.taskService.update(id, taskDto);
     }

}

class PreConditionFailedException extends RuntimeException {

     private static final long serialVersionUID = -7043944113105756846L;

     public PreConditionFailedException(String message) {
          super(message);
     }

}

@RestControllerAdvice
class ExceptionHandlerController {

     @ResponseStatus(code = HttpStatus.NOT_FOUND)
     @ExceptionHandler(value = EntityNotFoundException.class)
     public ErrorDto handleEntityNotFound(EntityNotFoundException entityNotFoundException) {
          return new ErrorDto(entityNotFoundException.getMessage(), HttpStatus.NOT_FOUND.value());
     }

     @ResponseStatus(code = HttpStatus.BAD_REQUEST)
     @ExceptionHandler(value = PreConditionFailedException.class)
     public ErrorDto handlePreConditionFailedException(PreConditionFailedException preConditionFailedException) {
          return new ErrorDto(preConditionFailedException.getMessage(), HttpStatus.BAD_REQUEST.value());
     }

}

interface TaskRepository extends JpaRepository<Task, Long> {

}

interface TaskService {

     TaskDto update(Long id, TaskDto taskDto);

}

@Service
class TaskServiceImpl implements TaskService {

     private final TaskRepository taskRepository;

     public TaskServiceImpl(TaskRepository taskRepository) {
          this.taskRepository = taskRepository;
     }

     @Override
     public TaskDto update(Long id, TaskDto taskDto) {
          if (!StringUtils.hasText(taskDto.getDescription())) {
               throw new PreConditionFailedException("Task description is required");
          }
          Task taskToUpdate = taskRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Cannot find task with given id"));
          BeanUtils.copyProperties(taskDto, taskToUpdate);
          taskRepository.saveAndFlush(taskToUpdate);

          return taskDto;
     }

}